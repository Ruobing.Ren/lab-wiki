echo "delete wikimediachanges topic if it exists"
$KAFKA_HOME/bin/kafka-topics.sh --delete --if-exists --bootstrap-server localhost:9092 --topic wikimediachanges
echo "create wikimediachanges topic"
$KAFKA_HOME/bin/kafka-topics.sh --create --bootstrap-server localhost:9092 --partitions 2 --config retention.ms=3600000 --topic wikimediachanges

echo "delete wikipediachanges topic if it exists"
$KAFKA_HOME/bin/kafka-topics.sh --delete --if-exists --bootstrap-server localhost:9092 --topic wikipediachanges
echo "create wikipediachanges topic"
$KAFKA_HOME/bin/kafka-topics.sh --create --bootstrap-server localhost:9092 --partitions 1 --config retention.ms=3600000 --topic wikipediachanges

